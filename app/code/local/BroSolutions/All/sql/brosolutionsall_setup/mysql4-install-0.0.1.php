<?php
$installer = $this;
$installer->startSetup();
$installer->addAttributeGroup('catalog_product', 'Default', 'Additional Product Details', 1000);

$installer->addAttribute('catalog_product', 'product_details', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'label' => 'Product Details',
    'position' => 25,
    'input' => 'textarea',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'wysiwyg_enabled'   => true,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
));
$installer->addAttribute('catalog_product', 'product_dimensions', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'source' => '',
    'label' => 'Product Dimensions',
    'position' => 30,
    'input' => 'textarea',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'wysiwyg_enabled'   => true,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
));
$installer->addAttribute('catalog_product', 'product_delivery_returns', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'label' => 'Delivery & Returns',
    'position' => 35,
    'input' => 'textarea',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'wysiwyg_enabled'   => true,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
));
$installer->addAttribute('catalog_product', 'recommended_retail_price', array(
    'group' => 'Prices',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Recommended Retail Price',
    'position' => 40,
    'input' => 'text',
    'class' => '',
    'source' => '',
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'user_defined' => true,
));

$installer->updateAttribute('catalog_product', 'product_details', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_product', 'product_details', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_details', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_details', 'used_in_product_listing', 1);

//used_in_product_listing
//is_visible_on_front

$installer->updateAttribute('catalog_product', 'product_dimensions', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_product', 'product_dimensions', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_dimensions', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_dimensions', 'used_in_product_listing', 1);

$installer->updateAttribute('catalog_product', 'product_delivery_returns', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_product', 'product_delivery_returns', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_delivery_returns', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_delivery_returns', 'used_in_product_listing', 1);


$installer->updateAttribute('catalog_product', 'recommended_retail_price', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'recommended_retail_price', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'recommended_retail_price', 'used_in_product_listing', 1);

$installer->endSetup();
