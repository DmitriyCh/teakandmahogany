<?php

$installer = $this;
$installer->startSetup();

$installer->removeAttribute('catalog_product', 'cpsp_show_stock');
$installer->removeAttribute('catalog_product', 'cpsp_update_fields');
$installer->removeAttribute('catalog_product', 'cpsp_enable');
$installer->removeAttribute('catalog_product', 'cpsp_enable_tier');
$installer->removeAttribute('catalog_product', 'cpsp_expand_prices');
$installer->removeAttribute('catalog_product', 'cpsp_show_last_price');
$installer->removeAttribute('catalog_product', 'cpsp_show_lowest');
$installer->removeAttribute('catalog_product', 'cpsp_show_maxregular');
$installer->removeAttribute('catalog_product', 'cpsp_use_preselection');
$installer->removeAttribute('catalog_product', 'cpsp_use_tier_lowest');

$installer->endSetup();

