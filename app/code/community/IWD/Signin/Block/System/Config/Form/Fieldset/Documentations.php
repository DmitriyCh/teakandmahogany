<?php
class IWD_Signin_Block_System_Config_Form_Fieldset_Documentations extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return '<span class="notice"><a href="https://docs.google.com/document/d/1qurAJKUTjw7n-HtfynL4oa2Pnm0MROmcMoz_SJzWTJg/" target="_blank">User Guide</span>';
    }
}