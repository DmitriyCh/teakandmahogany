<?php

class IWD_Signin_CaptchaController extends Mage_Core_Controller_Front_Action
{
    public function captchaReloadAction()
    {
        try {
            $result['src'] = Mage::helper('signin/captcha')->generateCaptchaImage();
            $result['error'] = '';
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'signin.log');
            $result['src'] = '';
            $result['error'] = $e->getMessage();
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function checkCaptchaAction()
    {
        $result['valid'] = 0;

        $captcha_text = $this->getRequest()->getParam('captcha_text');
        $captcha_code = Mage::helper('signin/captcha')->checkStandardCaptcha();

        $page = $this->getRequest()->getParam('page');
        $code = $this->getRequest()->getParam('g-recaptcha-response');

        if (isset($captcha_text) && !empty($captcha_text) && Mage::helper('signin/captcha')->checkCaptchaCode($captcha_text, $page)) {
            if($captcha_text == $captcha_code) {
                $result['valid'] = 0;
                $result['src'] = Mage::helper('signin/captcha')->generateCaptchaImage();
                $result['err_message'] = Mage::helper('signin/captcha')->__("IWD_Captcha is incorrect");
            }
        } else {
            $result['valid'] = 1;
        }

        if (isset($code) && !empty($code) && Mage::helper('signin/captcha')->checkCaptchaCode($code, $page)) {
            $result['valid'] = 0;
            $result['src'] = Mage::helper('signin/captcha')->generateCaptchaImage();
            $result['err_message'] = Mage::helper('signin/captcha')->__("reCaptcha is incorrect");
        } else {
            $result['valid'] = 1;

        }

//        $is_enable_recaptcha = false;
//
//        if($is_enable_recaptcha) {
//            $captcha = $this->checkReCaptcha();
//            $result = $result && $captcha;
//            if ($captcha == false) {
//                $result['err_message'] = Mage::helper('signin/captcha')->__("reCaptcha is incorrect Andrew Test");
//            }
//        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }


//    public function checkReCaptcha(){
//        require_once "recaptchalib.php";
//        $ccode = $_POST['g-recaptcha-response'];
//        $ip = $_SERVER['REMOTE_ADDR'];
//        $reCaptcha = new ReCaptcha(self::RE_CAPTCHA_SECRET);
//        $recaptchaResponse = $reCaptcha->verifyResponse($ip, $ccode);
//        return $recaptchaResponse->success;
//    }
}