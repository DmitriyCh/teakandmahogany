<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'recommended_retail_price_from', array(
    'group' => 'Prices',
    'type' => 'text',
    'backend' => '',
    'frontend' => '',
    'label' => 'Recommended Retail Price From',
    'position' => 45,
    'input' => 'text',
    'class' => '',
    'source' => '',
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'user_defined' => true,
    'sort_order' => 23,
));

$installer->updateAttribute('catalog_product', 'recommended_retail_price_from', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'recommended_retail_price_from', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'recommended_retail_price_from', 'used_in_product_listing', 1);

$installer->endSetup();

