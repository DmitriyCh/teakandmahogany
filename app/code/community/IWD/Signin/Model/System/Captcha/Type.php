<?php
/**
 * Created by PhpStorm.
 * User: AndrewChorniy
 * Date: 18.08.2015
 */

    class IWD_Signin_Model_System_Captcha_Type{
        public function toOptionArray()
        {
            return array(
                array(
                    "value" => IWD_Signin_Helper_Captcha::GOOGLE_RECAPTCHA,
                    "label" => Mage::helper('captcha')->__('Google ReCaptcha')
                ),
                array(
                    "value" => IWD_Signin_Helper_Captcha::IWD_CAPTCHA,
                    "label" => Mage::helper('captcha')->__('Standard Signin CAPTCHA')
                )
            );
        }
    }