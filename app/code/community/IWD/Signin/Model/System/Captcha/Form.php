<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chorniy
 * Date: 17.08.2015
 */

    class IWD_Signin_Model_System_Captcha_Form{

        public function toOptionArray()
        {
            return $testiwd = array(
                array(
                    'value' => IWD_Signin_Helper_Captcha::LOGIN_PAGE,
                    'label' => Mage::helper('captcha')->__('Login Form')
                ),
                array(
                    'value' => IWD_Signin_Helper_Captcha::FORGOT_PAGE,
                    'label' => Mage::helper('captcha')->__('Forgot Password  Form')
                ),
                array(
                    'value' => IWD_Signin_Helper_Captcha::REGISTER_PAGE,
                    'label' => Mage::helper('captcha')->__('Registration Form')
                ),
            );
        }
    }
?>