<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'product_details_title', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'label' => 'Product Details Title',
    'position' => 1,
    'input' => 'text',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'wysiwyg_enabled'   => true,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'sort_order' => 1,
));

$installer->addAttribute('catalog_product', 'product_details', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'label' => 'Product Details',
    'position' => 2,
    'input' => 'textarea',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'wysiwyg_enabled'   => true,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'sort_order' => 2,
));

$installer->addAttribute('catalog_product', 'product_dimensions_title', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'source' => '',
    'label' => 'Product Dimensions Title',
    'position' => 3,
    'input' => 'text',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'wysiwyg_enabled'   => true,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'sort_order' => 3,
));

$installer->addAttribute('catalog_product', 'product_dimensions', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'source' => '',
    'label' => 'Product Dimensions',
    'position' => 4,
    'input' => 'textarea',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'wysiwyg_enabled'   => true,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'sort_order' => 4,
));

$installer->addAttribute('catalog_product', 'product_delivery_returns_title', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'label' => 'Delivery, Returns & Care Title',
    'position' => 5,
    'input' => 'text',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'wysiwyg_enabled'   => true,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'sort_order' => 5,
));

$installer->addAttribute('catalog_product', 'product_delivery_returns', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'label' => 'Delivery, Returns & Care',
    'position' => 6,
    'input' => 'textarea',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'wysiwyg_enabled'   => true,
    'unique' => false,
    'visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'sort_order' => 6,
));

$installer->addAttribute('catalog_product', 'product_designer_talk_title', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'label' => 'Design Story Title',
    'position' => 7,
    'input' => 'text',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'is_wysiwyg_enabled'   => true,
    'unique' => false,
    'is_visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'used_in_product_listing' => true,
    'sort_order' => 7,
));

$installer->addAttribute('catalog_product', 'product_designer_talk', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'label' => 'Design Story',
    'position' => 8,
    'input' => 'textarea',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'is_wysiwyg_enabled'   => true,
    'unique' => false,
    'is_visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'used_in_product_listing' => true,
    'sort_order' => 8,
));

$installer->updateAttribute('catalog_product', 'product_details_title', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_details_title', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_details_title', 'used_in_product_listing', 1);

$installer->updateAttribute('catalog_product', 'product_details', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_product', 'product_details', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_details', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_details', 'used_in_product_listing', 1);

$installer->updateAttribute('catalog_product', 'product_dimensions_title', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_dimensions_title', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_dimensions_title', 'used_in_product_listing', 1);

$installer->updateAttribute('catalog_product', 'product_dimensions', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_product', 'product_dimensions', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_dimensions', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_dimensions', 'used_in_product_listing', 1);

$installer->updateAttribute('catalog_product', 'product_delivery_returns_title', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_delivery_returns_title', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_delivery_returns_title', 'used_in_product_listing', 1);

$installer->updateAttribute('catalog_product', 'product_delivery_returns', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_product', 'product_delivery_returns', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_delivery_returns', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_delivery_returns', 'used_in_product_listing', 1);

$installer->updateAttribute('catalog_product', 'product_designer_talk_title', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_designer_talk_title', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_designer_talk_title', 'used_in_product_listing', 1);

$installer->updateAttribute('catalog_product', 'product_designer_talk', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_product', 'product_designer_talk', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_designer_talk', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_designer_talk', 'used_in_product_listing', 1);

$installer->endSetup();

