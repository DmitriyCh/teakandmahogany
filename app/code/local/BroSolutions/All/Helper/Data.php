<?php

class BroSolutions_All_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isProductNew(Mage_Catalog_Model_Product $product)
    {
        $newsFromDate = $product->getNewsFromDate();
        $newsToDate   = $product->getNewsToDate();
        if (!$newsFromDate && !$newToDate) {
            return false;
        }
        return Mage::app()->getLocale()
                ->isStoreDateInInterval($product->getStoreId(), $newsFromDate, $newsToDate);
    }
}
