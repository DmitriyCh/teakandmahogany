<?php
$installer = $this;
$installer->startSetup();
$content = 'Lorem ipsum dolor sit amet';
$stores = Mage::getModel('core/store')->getCollection()->addFieldToFilter('store_id', array('gt'=>0))->getAllIds();
foreach ($stores as $store){
    $block = Mage::getModel('cms/block');
    $block->setTitle('Retail Price Description');
    $block->setIdentifier('retail_price_description');
    $block->setStores(array($store));
    $block->setIsActive(1);
    $block->setContent($content);
    $block->save();
}
$installer->endSetup();
