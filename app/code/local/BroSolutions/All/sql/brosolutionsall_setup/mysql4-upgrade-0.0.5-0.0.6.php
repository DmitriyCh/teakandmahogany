<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'product_designer_talk', array(
    'group' => 'Additional Product Details',
    'type' => 'text',
    'backend' => '',
    'label' => 'Design Story',
    'position' => 60,
    'input' => 'textarea',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'is_wysiwyg_enabled'   => true,
    'unique' => false,
    'is_visible_on_front' => true,
    'is_html_allowed_on_front' => true,
    'used_in_product_listing' => true,
    'sort_order' => 4,
));

$installer->updateAttribute('catalog_product', 'product_designer_talk', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_product', 'product_designer_talk', 'is_html_allowed_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_designer_talk', 'is_visible_on_front', 1);
$installer->updateAttribute('catalog_product', 'product_designer_talk', 'used_in_product_listing', 1);

$installer->endSetup();

