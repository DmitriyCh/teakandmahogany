<?php
//require_once "recaptchalib.php";

class IWD_Signin_Helper_Captcha extends Mage_Core_Helper_Abstract
{
    const LOGIN_PAGE = 'login';
    const REGISTER_PAGE = 'register';
    const FORGOT_PAGE = 'forgot';
    const XML_PATH_ENABLED_CAPTCHA = 'signin/captcha/enable';
    const GOOGLE_RECAPTCHA = 'google';
    const IWD_CAPTCHA = 'standard';

    const GOOGLE_CAPTCHA_PUBLIC_KEY = 'signin/captcha/public_key';
    const GOOGLE_CAPTCHA_PRIVATE_KEY = 'signin/captcha/private_key';
    const IWD_CAPTCHA_LETTERS = 'signin/captcha/symbols';
    const IWD_CAPTCHA_SYMBOLS_NUMBER = 'signin/captcha/length';
    const IWD_CAPTCHA_FON_LINES_NUMBER = 'signin/captcha/fon_amount';
    const IWD_CAPTCHA_FORM = 'signin/captcha/forms';
    const IWD_CAPTCHA_TYPE = 'signin/captcha/choose_captcha';
    const IWD_CAPTCHA_ERROR_MESSAGE = 'signin/captcha/error_message';

    const RE_CAPTCHA_SECRET = "6LerXgoTAAAAAKAgxWyXYcQqzjEeN0M3Ia9dj1D3";

    protected $width = 150;
    protected $height = 60;
    protected $font_size = 15;
    public $let_amount;
    protected $fon_amount;
    protected $font = "skin/frontend/base/default/css/iwd/signin/fonts/LDFComicSans.ttf";
    protected $letters;
    protected $colors = array("90", "110", "130", "150", "170", "190", "210");
    protected $image;



    protected $afterIncorrectCountTrying = 0;

    public function  isCaptchaEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLED_CAPTCHA) ? true : false;
    }


    public function generateCaptchaImage()
    {
        $this->initImage();

        $this->addLinesToFon();

        $code = $this->addCodeToImage();

        Mage::getSingleton('core/session')->setIwdSigninCommentCaptchaCode($code);

        return $this->saveFile();
    }

    public function isGoogleReCaptcha(){
        return (Mage::getStoreConfig(self::IWD_CAPTCHA_TYPE) == 'google');
    }

    public function getCaptcha($page)
    {
        if (!$this->isEnabledCaptchaOnPage($page)) {
            return '';
        }

        $count = $this->getCountTried();
        if ($count >= $this->afterIncorrectCountTrying) {
            if ($this->isGoogleReCaptcha()) {
                return $this->getGoogleCaptcha();
            }

            return $this->getCaptchaImage();
        }

        return '';
    }

    protected function isEnabledCaptchaOnPage($page)
    {
        return 1; //TODO:
    }

    protected function getCountTried()
    {
        return Mage::getSingleton('core/session')->getData('iwd_sigin_login_count_tried');
    }

    protected function setCountTried($count)
    {
        return Mage::getSingleton('core/session')->setData('iwd_sigin_login_count_tried', $count);
    }


    public function checkCaptchaCode($code, $page)
    {
        $result = ($this->isGoogleReCaptcha()) ? $this->checkGoogleCaptcha($code) : $this->checkStandardCaptcha($code);
        if ($page == self::LOGIN_PAGE) {
            if (!$result) {
                $count = $this->getCountTried() + 1;
                $this->setCountTried($count);
            } else {
                $this->setCountTried(0);
            }
        }

        return $result;
    }

    public function checkPrivateKey(){
        return Mage::getStoreConfig(self::GOOGLE_CAPTCHA_PRIVATE_KEY);
    }

    public function checkStandardCaptcha()
    {
        $code = Mage::getSingleton('core/session')->getIwdSigninCommentCaptchaCode();
        return $code;
    }

    public function isCaptchaOnRegisterPage()
    {
        $checkForm = Mage::getStoreConfig(self::IWD_CAPTCHA_FORM);

        $checkForm = explode(',', $checkForm);
        return in_array(self::REGISTER_PAGE, $checkForm);

    }

    public function isCaptchaOnLoginPage()
    {
        $checkForm = Mage::getStoreConfig(self::IWD_CAPTCHA_FORM);

        $checkForm = explode(',', $checkForm);
        return in_array(self::LOGIN_PAGE, $checkForm);
    }

    public function isCaptchaOnForgotPasswordPage()
    {
        $checkForm = Mage::getStoreConfig(self::IWD_CAPTCHA_FORM);

        $checkForm = explode(',', $checkForm);
        return in_array(self::FORGOT_PAGE, $checkForm);
    }

    protected function getCaptchaImage()
    {
        $img = $this->generateCaptchaImage();

        return sprintf('
 		<div class="comment-captcha iwd-sigin-capcha-test">
 		    <label for="pass" class="required">%s<em>*</em></label>
 			<img id="comment-captcha"
 				 src="%s"
 				 title="%s"/>
 			<div id="iwd-comment-captcha-load" style="display:none;"></div>
 			<div class="clear"></div>
 			<label>%s</label>
 			<input type="text" name="captcha_text" class="comment-captcha-text" required="required" maxlength="%s"/>
            <div id="iwd-signin-captcha-error" class="hide"></div>
 		</div>',
            $this->__('Captcha'),
            $img,
            $this->__('Click for update captcha'),

            $this->__("Please type the symbols from picture above"),
            Mage::getStoreConfig(self::IWD_CAPTCHA_SYMBOLS_NUMBER)
        );

    }

    protected function getGoogleCaptcha()
    {
        $key = Mage::getStoreConfig(self::GOOGLE_CAPTCHA_PUBLIC_KEY);

        return sprintf('<script src="https://www.google.com/recaptcha/api.js?hl=en"></script>
         <div class="captcha-input">
             <label for="pass" class="required">%s<em>*</em></label>
             <div class="g-recaptcha" data-sitekey="%s"></div>
            <div id="iwd-signin-captcha-error" class="hide"></div>
             </div>',
            $this->__('Captcha'),
            $key);
 
     }

    protected function addCodeToImage()
    {
        $cod = array();
        for ($i = 0; $i < $this->let_amount = Mage::getStoreConfig(self::IWD_CAPTCHA_SYMBOLS_NUMBER); $i++) {
            $color = imagecolorallocatealpha($this->image, $this->colors[rand(0, sizeof($this->colors) - 1)],
                $this->colors[rand(0, sizeof($this->colors) - 1)],
                $this->colors[rand(0, sizeof($this->colors) - 1)], rand(20, 40));
            $letter = $this->letters[rand(0, strlen($this->letters = Mage::getStoreConfig(self::IWD_CAPTCHA_LETTERS)) - 1)];
            $size = rand($this->font_size * 2 - 2, $this->font_size * 2 + 2);
            $x = ($i) * $this->font_size * 1.5 + rand(1, 10);
            $y = (($this->height * 2) / 3) + rand(0, 5);
            $cod[] = $letter;
            imagettftext($this->image, $size, rand(0, 15), $x, $y, $color, $this->font, $letter);
        }
        return implode("", $cod);
    }

    protected function initImage()
    {
        $this->image = imagecreatetruecolor($this->width, $this->height);
        $fon = imagecolorallocate($this->image, 255, 255, 255);
        imagefill($this->image, 0, 0, $fon);
    }

    protected function saveFile()
    {
        $filename = uniqid() . ".gif";
        $file_dir = $this->getMediaCaptureDir() . $filename;
        imagegif($this->image, $file_dir);
        return Mage::getBaseUrl('media') . 'captcha/signin/' . $filename;
    }

    protected function addLettersToFon()
    {
        for ($i = 0; $i < $this->fon_amount = Mage::getStoreConfig(self::IWD_CAPTCHA_FON_LINES_NUMBER); $i++) {
            $color = imagecolorallocatealpha($this->image, rand(0, 255), rand(0, 255), rand(0, 255), 100);
            $letter = $this->letters[rand(0, strlen($this->letters = Mage::getStoreConfig(self::IWD_CAPTCHA_LETTERS)) - 1)];
            $size = rand($this->font_size - 2, $this->font_size + 2);
            imagettftext($this->image, $size, rand(0, 45),
                rand($this->width * 0.1, $this->width - $this->width * 0.1),
                rand($this->height * 0.2, $this->height), $color, $this->font, $letter);
        }
    }

    protected function addLinesToFon()
    {
        for ($i = 0; $i < $this->fon_amount = Mage::getStoreConfig(self::IWD_CAPTCHA_FON_LINES_NUMBER); $i++) {
            imagesetthickness($this->image, rand(1, 3));
            $color = imagecolorallocatealpha($this->image, rand(0, 255), rand(0, 255), rand(0, 255), 100);
            imageline($this->image, rand(0, $this->width), rand(0, $this->height), rand(0, $this->width), rand(0, $this->height), $color);
        }
    }

    protected function getMediaCaptureDir()
    {
        $path = Mage::getBaseDir('media') . DS . 'captcha' . DS;
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }

        $path .= 'signin' . DS;
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }

        return $path;
    }
}